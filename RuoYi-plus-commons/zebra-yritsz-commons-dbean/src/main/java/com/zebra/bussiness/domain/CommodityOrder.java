package com.zebra.bussiness.domain;

import com.zebra.common.annotation.Excel;
import com.zebra.common.core.domain.BussinessEntity;
import com.zebra.common.core.domain.MyNextVersion;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import tk.mybatis.mapper.annotation.Version;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * t_commodity_order表 CommodityOrder
 * 
 * @author zebra
 * @date 2020-08-18
 */
@Table(name="t_commodity_order")
@Getter
@Setter
@ToString
public class CommodityOrder extends BussinessEntity {
    private static final long serialVersionUID = 1L;
	
    /** 订单编号 */
     @Excel(name = "订单编号")
     @Id
     private String orderId;

    /** 订单金额(分) */
     @Excel(name = "订单金额(分)")
     @Column(name="order_money")
     private Long orderMoney;

    /** 订单状态（1待支付 2支付成功 3支付失败 4退款申请 5退款） */
     @Excel(name = "订单状态", readConverterExp = "1=待支付,2=支付成功,3=支付失败,4=退款申请,5=退款")
     @Column(name="order_status")
     private Integer orderStatus;

    /** 用户姓名 */
     @Excel(name = "用户姓名")
     @Column(name="user_name")
     private String userName;

    /** 用户电话 */
     @Excel(name = "用户电话")
     @Column(name="user_phone")
     private String userPhone;

    /** 用户地址 */
     @Excel(name = "用户地址")
     @Column(name="user_address")
     private String userAddress;

    /** 产品id */
     @Excel(name = "产品id")
     @Column(name="commodity_id")
     private String commodityId;

    /** 产品数量 */
     @Excel(name = "产品数量")
     @Column(name="commodity_count")
     private Integer commodityCount;

    /** 商户id */
     @Excel(name = "商户id")
     @Column(name="merchant_id")
     private String merchantId;

    /** 创建时间 */
     @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
     @Column(name="create_time")
     private Date createTime;

    /** 更新时间 */
     @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
     @Column(name="update_time")
     private Date updateTime;

    /** 操作人 */
     @Excel(name = "操作人")
     @Column(name="update_by")
     private String updateBy;

    /** 数据版本 */
    @Version(nextVersion = MyNextVersion.class)
    @Column(name="data_ver_flag")
     private Long dataVerFlag;

}
